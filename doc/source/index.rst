.. JuliaUnit documentation master file, created by
   sphinx-quickstart on Thu Aug 14 16:02:54 2014.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to JuliaUnit's documentation!
=====================================

JuliaUnit extends the basic testing functions in Julia in two ways. First, the
tests can be more structured by allowing test cases to be grouped in test classes
and packages. Second, an XML file is generated in JUnit style. This way, the
reports can be read easily by CI systems such as `Jenkins
<http://jenkins-ci.org/>`_.

Contents
=========

.. toctree::
   :maxdepth: 2

   quickstart
   manual
   functions

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

