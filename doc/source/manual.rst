.. _manual:

***************************
 Manual
***************************

###############################
 Structuring and running tests
###############################

The basic method to run tests is ``runtests()``. This will run all tests it can
find in ``.jl`` files starting with ``test``, e.g. ``testFoo.jl``. The method
will look for test files in the current directory and in all subdirectories. For
this manual, we assume that the current directory is called ``test``. Each
subdirectory is a test package, each test file is a test class and each test class
can contain multiple test cases. 

A *test case* is the core unit of the JuliaUnit test framework. The main goal of
a test case is to test if a function behaves as expected. To test this, multiple
tests can be used. Failure of one test leads to a failure of the test case. The
first test that fails, will be reported in the report. Tests following a failed
tests in a test case will be executed regardless of the previous tests. For
example:

.. code-block:: julia

   @testcase "Bar" begin
       f = x -> x*x
       @test f(2) == 4
       @test f(2) == 5    
       @test f(1) == 2    
   end

This will result in:

.. code-block:: xml

   <testcase classname="Foo" name="Bar" time="0.011">
      <failure message="Test failed" type="assertionFailed">Failure on: f(2) == 5</failure>
      <failure message="Test failed" type="assertionFailed">Failure on: f(1) == 2</failure>
   </testcase>

Multiple test cases can be grouped in a *test class*. A test class is defined by
the filename of the test file. The filename of each test file must be of the
form ``testXXX.jl`` where ``XXX`` needs to be replaced with the name of the
class. Test classes can be grouped together in *test packages* by placing the
files in a subdirectory of ``test``. Directories can be nested to group multiple
test packages. For example, suppose we have two test files ``test/testFoo.jl`` and
``test/AA/testBB.jl`` and we run ``runtests()`` from the ``test`` directory,
then we get the following output:

.. code-block:: xml

   <?xml version="1.0" encoding="utf-8"?>
   <testsuite>
     <testcase classname="Foo" name="Bar" time="0.019">
       <failure message="Test failed" type="assertionFailed">Failure on: f(2) == 5</failure>
       <failure message="Test failed" type="assertionFailed">Failure on: f(1) == 2</failure>
     </testcase>
     <testcase classname="AA.BB" name="Bar" time="0.006">
       <failure message="Test failed" type="assertionFailed">Failure on: f(2) == 5</failure>
       <failure message="Test failed" type="assertionFailed">Failure on: f(1) == 2</failure>
     </testcase>
   </testsuite>

The results of the tests can be saved to an ``.xml`` file by using the
``savesuite(filename)`` function or by passing a filename to the ``runtests()``
command:

.. code-block:: julia

   savesuite("results.xml")
   runtests("results.xml")

#################
 Available tests
#################

The following test macros are available to use inside a test case:

- ``@test`` for tests with a boolean outcome
- ``@testeq`` for testing if two values are equal
- ``@testapproxeq`` for testing if two values are equal within a certain
  (relative) accuracy
- ``@testapproxabseq`` for testing if two values are equal within a certain
  absolute accuracy
- ``@testthrows`` for testing if an error is thrown
- ``@testthrowsspecific`` for testing if a specific error is thrown

#####################################
Running tests without ``runtests()``
#####################################

Sometimes constructing the whole ``test`` directory with multiple files is too
much work. Each test case will automatically run when a file is included, or
when typed in the interactive Julia prompt. For example:

.. code-block:: julia
   
   julia> @testcase "Foo" begin
              @test 1==1
          end

   Case 'Foo':  ......................................................... Success in 0.002 s 

All tests are recorded automatically, so the following commands can be used:

.. code-block:: julia
   
   julia> @summarizeclass

   Class summary: 1 tests, 0 failures.

   julia> @summarizesuite

   Suite summary: 1 tests, 0 failures.

   julia> getxml()

   <?xml version="1.0" encoding="utf-8"?>
   <testsuite tests="1" failures="0">
     <testcase classname="Auto" name="Foo" time="0.010"/>
   </testsuite>

   julia> savesuite("results.xml")

As can be seen in the example above, all tests are grouped under the ``Auto``
class. This can be changed using the ``@testclass`` macro:

.. code-block:: julia
 
   julia> @testclass "Bar"

   julia> @testcase "Foo" begin
              @test 1==1
          end
  
   julia> getxml()
           
   <?xml version="1.0" encoding="utf-8"?>
   <testsuite tests="1" failures="0">
     <testcase classname="Bar" name="Foo" time="0.010"/>
   </testsuite>

When you have tried the examples above, you will see that the :jl:func:`getxml`
result will contain all tests. To reset the test recording :jl:func:`resetxml` can
be used.
