***********
 Functions
***********

#############
 Test macros
#############

.. function:: @test(ex)

   Report 
              
   :param Expr ex: Expression to be evaluated. Must return ``true`` or ``false``

.. function:: @testeq(ex1, ex2)

   Test if :param:`ex1`

.. function:: @testapproxeq(ex1, ex2, eps)
              
   Test if ``ex1`` and ``ex2`` approximately evaluate to the same value in the
   relative sense, i.e. ``norm(ex1 - ex2)/norm(ex2) < eps``.

   :param Expr ex1: Expression to test
   :param Expr ex2: Expression to test to
   :param Number eps: Requested relative accuracy


.. function:: runtests()

   comments, bla

.. function:: filterwarnings(action, message='', category=Warning, \
                             module='', lineno=0, append=False)
