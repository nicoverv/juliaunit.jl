.. _quickstart:

***************************
 Quickstart
***************************

##############
 Installation
##############

Currently, the package is not registered as an official package, but it can be
installed using the built-in package manager: 

.. code-block:: julia

   Pkg.clone("https://bitbucket.org/nicoverv/JuliaUnit.jl.git") # Keep capitals!

This package depends on the `LightXML
<https://github.com/lindahua/LightXML.jl>`_ package, which also can be installed
using the package manager:

.. code-block:: julia

   Pkg.add("LightXML")

To run the tests for this the JuliaUnit package, you will also need the `Match
<https://github.com/kmsquire/Match.jl>`_ package, which can be installed in the
same way as LightXML.


#########
 Example
#########

Suppose you have a test file called ``testFoo.jl``: 

.. code-block:: julia

   @testcase "Bar" begin
       f = x -> x*x
       @test f(2) == 4
   end

   @testcase "Bla" begin
       f = x -> x*x
       @test f(2) == 5
   end

You can now run the tests using the ``runtests()`` function:

.. code-block:: julia

   using JuliaUnit

   runtests();
   
This will generate the following output

.. code-block:: julia

   Class Foo:
       Case 'Bar':  ......................................................... Success in 0.005 s 
       Case 'Bla':  ......................................................... Failure in 0.013 s 
   Summary: 2 tests, 1 failures.

The results can be saved to an XML file using, or ``runtests`` can be called
with a filename as argument.

.. code-block:: julia

   savesuite("testresults.xml")
   # runtests("testresults.xml")

The resulting ``testresults.xml`` file looks as follows:

.. code-block:: xml

   <?xml version="1.0" encoding="utf-8"?>
   <testsuite>
     <testcase classname="Foo" name="Bar" time="0.005"/>
     <testcase classname="Foo" name="Bla" time="0.013">
       <failure message="Test failed" type="assertionFailed">Failure on: f(2) == 5</failure>
     </testcase>
   </testsuite>


