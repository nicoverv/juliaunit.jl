# JuliaUnit
# ---------
# Unit testing package for Julia (julialang.org). The test results are summarized in
# the command window and in an XML file in JUnit style. This way the test results can
# be incorporated in a continuous integration system that accepts JUnit files.
#
# Author: Nico Vervliet
# Version History:
# - 2014/03/04    NV   Initial version
module JuliaUnit

using LightXML

export @test,
       @testcase,
       @testclass,
       @testeq,
       @testapproxeq,
       @testapproxabseq,
       @testthrows,
       @testthrowsspecific,
       @summarizeclass,
       @summarizesuite,

       getxml,
       runtests,
       runtestfile,
       savesuite,
       resetxml


# All test outcomes are of the type result, inheritance is used to select the right handler.
abstract Result

type Success <: Result
    expr
end
type Failure <: Result
    expr
    message
    reason
    failuretype

    Failure(ex) = new(ex, "Test failed", "", "assertionFailed")
    Failure(ex, message, reason) = new(ex, message, reason, "assertionFailed")
    Failure(ex, message, reason, failuretype) = new(ex, message, reason, failuretype)
end
type Error <: Result
    expr
    err
    backtrace
end

# XML
currentsuite = nothing
type Suite
    nbCases
    nbFailures
    doc
    root

    function Suite()
        doc = XMLDocument();
        root = create_root(doc, "testsuite")
        currentsuite = new(0, 0, doc, root)
    end
end

function getxml()
    global currentsuite
    if currentsuite == nothing
        nothing
    else
        set_attribute(currentsuite.root, "tests", "$(currentsuite.nbCases)")
        set_attribute(currentsuite.root, "failures", "$(currentsuite.nbFailures)")
        currentsuite.doc
    end
end
    

# Handlers
handler(r::Success) = print("Success")
function handler(r::Failure)
    global currentsuite
    global currentclass
    if currentclass !== nothing
        currentclass.nbFailures += 1
        currentsuite.nbFailures += 1
        fail = new_child(currentclass.lastcase, "failure")
        set_attribute(fail, "message", r.message)
        set_attribute(fail, "type", r.failuretype)
        if r.reason != ""
            add_text(fail, "Failure on: $(r.expr)\n\n$(r.reason)")
        else
            add_text(fail, "Failure on: $(r.expr)")
        end
    end
    print("Failure")
end
function handler(r::Error)
    r.backtrace = sprint(io->Base.show_backtrace(io, r.backtrace))
    global currentsuite
    global currentclass
    if currentclass !== nothing
        currentclass.nbFailures += 1
        currentsuite.nbFailures += 1
        fail = new_child(currentclass.lastcase, "error")
        set_attribute(fail, "message", "$(r.err)")
        if r.backtrace != ""
            add_text(fail, "Error on: $(r.expr)\n\n$(r.backtrace)")
        else
            add_text(fail, "Error on: $(r.expr)")
        end
    end
    print("Error  ")
end


# Classes
currentclass = nothing
type Class
    name
    nbCases
    nbFailures

    # XML stuff
    node
    lastcase
    
    function Class(name)
        global currentsuite
        global currentclass
        print("Class $name:\n")
        if currentsuite == nothing
            currentsuite = Suite()
        end
        node = currentsuite.root
        
        currentclass = new(name, 0, 0, node)
    end
end

# Cases
type Case
    function Case(name, ex)
        global currentclass
        indent = ""
        if currentclass == nothing
            Class("Auto")
        end 
        if currentclass !== nothing
            indent = "    "
            currentclass.nbCases += 1
            currentsuite.nbCases += 1
            node = new_child(currentclass.node, "testcase")
            set_attribute(node, "classname", currentclass.name)
            set_attribute(node, "name", name)
            currentclass.lastcase = node
        end
        print("$(indent)Case '$name': ")
        print(" "*repeat(".", 60-length(name))*" ")
        t0 = time()
        ex()
        t1 = time()
        @printf(" in %0.3f s \n", t1-t0)
        if currentclass !== nothing
            set_attribute(currentclass.lastcase, "time", @sprintf("%0.3f",t1-t0))
        end
        ()
    end
end

# macros
macro testcase(name, ex)
    :(Case($(esc(name)), ()->($(esc(ex)))))
end

function do_test(body,qex)
    handler(try
        body() ? Success(qex)  : Failure(qex)
    catch err
        Error(qex,err,catch_backtrace())
    end)
end

function do_test_throws(body,qex)
    handler(try
        body()
        Failure(qex, "No exception thrown", "", "noErrorThrown")
    catch err
        Success(qex)
    end)
end

function do_test_throws_specific(body,testerr,qex)
    handler(try
        body()
        Failure(qex, "No exception thrown", "Expected $testerr", "noErrorThrown")
    catch err
        if isa(err, testerr)
            Success(qex)
        else
            backtrace = catch_backtrace()
            Failure(qex, "Wrong exception thrown", "Expected $testerr, but got $err instead.\n\n$backtrace", "wrongErrorThrown")
        end
    end)
end

function do_test_approx(exleft,exright,eps,qexleft, qexright)
    handler(try
        a = exleft()
        b = exright()
        relerr = norm(a-b)/norm(b)
        expr = "$(qexleft) = $(qexright)"
        if relerr < eps
            Success(expr)
        else
            expr = "$(qexleft) = $(qexright)"
            message = "Relative error is larger than requested."
            reason = @sprintf("Left hand side:\n  %s = \n%s\n\nRight hand side: \n%s = \n%s\n\nRelative error:                                %6.3e\nRequested tolerance:                           %6.3e",
                              "$(qexleft)", string(a),
                              "$(qexright)", string(b), relerr, eps)
            Failure(expr, message, reason, "assertionFailed:tolexceeded")
        end
    catch err
        show(err)
        print(err)
        Error(qexleft,err,catch_backtrace())
    end)
end

function do_test_approx_abs(exleft,exright,eps,qexleft, qexright)
    handler(try
        a = exleft()
        b = exright()
        abserr = norm(a-b)
        expr = "$(qexleft) = $(qexright)"
        if abserr < eps
            Success(expr)
        else
            expr = "$(qexleft) = $(qexright)"
            message = "Absolute error is larger than requested."
            reason = @sprintf("Left hand side:\n  %s = \n%s\n\nRight hand side: \n%s = \n%s\n\Absolute error:                                %6.3e\nRequested tolerance:                           %6.3e",
                              "$(qexleft)", string(a),
                              "$(qexright)", string(b), abserr, eps)
            Failure(expr, message, reason, "assertionFailed:tolexceeded")
        end
    catch err
        show(err)
        print(err)
        Error(qexleft,err,catch_backtrace())
    end)
end

macro test(ex)
    :(do_test(()->($(esc(ex))),$(Expr(:quote,ex))))
end

macro testeq(ex, ex2)
    :(do_test(()->($(esc(ex)) == $(esc(ex2))),$(Expr(:quote,ex,==,ex2))))
end

macro testapproxeq(ex, ex2, eps)
    :(do_test_approx(()->($(esc(ex))),()->($(esc(ex2))),$(esc(eps)),$(Expr(:quote,ex)),$(Expr(:quote,ex2))))
end

macro testapproxabseq(ex, ex2, eps)
    :(do_test_approx_abs(()->($(esc(ex))),()->($(esc(ex2))),$(esc(eps)),$(Expr(:quote,ex)),$(Expr(:quote,ex2))))
end

macro testthrows(ex)
    :(do_test_throws(()->($(esc(ex))),$(Expr(:quote,ex))))
end

macro testthrowsspecific(ex, err)
    :(do_test_throws_specific(()->($(esc(ex))),$(esc(err)),$(Expr(:quote,ex))))
end

macro testclass(name)
    :(Class($(esc(name))))
end

macro summarizeclass()
    global currentclass
    if currentclass !== nothing
        print("Class summary: $(currentclass.nbCases) tests, $(currentclass.nbFailures) failures.\n\n")
    end
end

macro summarizesuite()
    global currentsuite
    if currentsuite !== nothing
        print("\nSuite summary: $(currentsuite.nbCases) tests, $(currentsuite.nbFailures) failures.\n\n")
    end
end

# Run tests in directory
function runtests(; reset = true)
    if reset; resetxml(); end
    
    dirs = readdir()
    testdirs = filter(r"^[a-zA-Z0-9]+$", dirs)

    testdirs = [".", testdirs...]

    for dir in testdirs
        try 
            files = readdir(dir)
            testfiles = filter(r"^test.+\.jl$", files)

            for file in testfiles
                runtestfile(file, dir)
            end
        catch
            print("some error")
            # do something ?
        end
    end
    currentsuite.doc
end

function runtestfile(file, dir)
    package_name = dir == "." ? "" : "$dir."
    
    try
        Class("$package_name$(file[5:end-3])")
        filename = dir == "." ? "$file" : "$dir/$file"
        include(filename)
        print("Summary: $(currentclass.nbCases) tests, $(currentclass.nbFailures) failures.\n\n")
    catch err
        show(err)
    end
end

runtestfile(file) = runtestfile(file, ".")

function runtests(filename; reset = true)
    runtests(reset = reset)
    savesuite(filename)
    currentsuite.doc
end 

savesuite(filename) = save_file(currentsuite.doc, filename)

function resetxml()
    global currentsuite
    global currentclass
    currentsuite = nothing
    currentclass = nothing
end

end

