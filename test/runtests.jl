include("../src/JuliaUnit.jl")

using LightXML
using Match
using JuliaUnit
import JuliaUnit.*

# Run the test suite
runtests()
savesuite("tests.xml")

# Reset the xml report
resetxml()

# Test the correctness of the previous suite
doc = parse_file("tests.xml")

# for output sanity, we remember the previous classname
prevclassname = ""

function startclass(classname)
    global prevclassname
    if classname != prevclassname
        @testclass classname
        prevclassname = classname
    end
end

# test if success
function isSuccess(case)
    length(collect(child_elements(case))) == 0
end

function testSuccess(case)
    startclass(attribute(case, "classname"))
    @testcase attribute(case, "name") begin
        @test isSuccess(case)
    end
end

function isFailure(case, failuretype)
    failures = collect(child_elements(case))
    for failure in failures
        if name(failure) != "failure" || attribute(failure, "type") != failuretype
            return false
        end
    end
    length(failures) > 0
end

function testFailure(case, failure)
    startclass(attribute(case, "classname"))
    @testcase attribute(case, "name") begin
        @test isFailure(case, failure)
    end
end

testcases = get_elements_by_tagname(root(doc), "testcase") 
for case in testcases
    classname = attribute(case, "classname")
    testname = attribute(case, "name")
    fullname = "$(classname).$(testname)"
    
    @match fullname begin
        "JuliaUnit.Condition.Test" => testSuccess(case)
        "JuliaUnit.Condition.FailedTest" => testFailure(case, "assertionFailed")
        "JuliaUnit.Equal.Equal" => testSuccess(case)
        "JuliaUnit.Equal.NotEqual" => testFailure(case, "assertionFailed")
        "JuliaUnit.Equal.ApproxRelativeEqual" => testSuccess(case)
        "JuliaUnit.Equal.ApproxRelativeNotEqual" => testFailure(case, "assertionFailed:tolexceeded")
        "JuliaUnit.Equal.ApproxRelativeEqualVector" => testSuccess(case)
        "JuliaUnit.Equal.ApproxRelativeNotEqualVector" => testFailure(case, "assertionFailed:tolexceeded")
        "JuliaUnit.Equal.ApproxAbsoluteEqual" => testSuccess(case)
        "JuliaUnit.Equal.ApproxAbsoluteNotEqual" => testFailure(case, "assertionFailed:tolexceeded")
        "JuliaUnit.Equal.ApproxAbsoluteEqualVector" => testSuccess(case)
        "JuliaUnit.Equal.ApproxAbsoluteNotEqualVector" => testFailure(case, "assertionFailed:tolexceeded")
        "JuliaUnit.Error.ThrowError" => testSuccess(case)
        "JuliaUnit.Error.ThrowSpecificError" => testSuccess(case)
        "JuliaUnit.Error.ThrowWrongError" => testFailure(case, "wrongErrorThrown")
        "JuliaUnit.Error.ThrowNoError" => testFailure(case, "noErrorThrown")
        "JuliaUnit.Error.ThrowNoErrorSpecific" => testFailure(case, "noErrorThrown")
        "JuliaUnit.Internals.ClassNameFromExpression" => testSuccess(case)
        "JuliaUnit.Internals.ClassNameFromString" => testSuccess(case)
        "JuliaUnit.Internals.NumberOfCases" => testSuccess(case)
        "JuliaUnit.Internals.NumberOfFailedCases" => testSuccess(case)
        "JuliaUnit.Internals.GenerateOutputFile" => testSuccess(case)
        "JuliaUnit.Internals.DummyFail" => nothing
        _ => print("Unexpected test '$fullname'...\n")
    end
end

@summarizesuite

savesuite("JuliaUnitTests.xml")

