f = x -> x.*x

@testcase "Equal" begin
    @testeq f(2) 4
end
    
@testcase "NotEqual" begin
    @testeq f(2) 5
end

@testcase "ApproxRelativeEqual" begin
    @testapproxeq f(100) 10001 1e-4
end

@testcase "ApproxRelativeNotEqual" begin
    @testapproxeq f(100) 10001 1e-5
end

@testcase "ApproxRelativeEqualVector" begin
    @testapproxeq f([100 100]) [10001 10001] 1e-4
end

@testcase "ApproxRelativeNotEqualVector" begin
    @testapproxeq f([100 100]) [10001 10001] 1e-5
end

@testcase "ApproxAbsoluteEqual" begin
    @testapproxabseq f(100) 10000.01 2e-2
end

@testcase "ApproxAbsoluteNotEqual" begin
    @testapproxabseq f(2) 10000.01 2e-5
end

@testcase "ApproxAbsoluteEqualVector" begin
    @testapproxabseq f([100 100]) [10000.01 10000.01] 2e-2
end

@testcase "ApproxAbsoluteNotEqualVector" begin
    @testapproxabseq f([2 2]) [10000.01 10000.01] 2e-5
end
