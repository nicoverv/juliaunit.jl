# This file tests the interals of the project and has to be handled first and last
using LightXML

try
    ;rm("tests.xml")
catch

end

# Case 1
base = "JuliaUnit"
class = "Internals"
@testclass "$(base).$(class)"

@testcase "ClassNameFromExpression" begin
    currentclass = JuliaUnit.currentclass
    @test currentclass.name == "JuliaUnit.Internals"
end

# Correct name in case it failed
@testclass "JuliaUnit.Internals"

# Case 2
@testcase "ClassNameFromString" begin
    currentclass = JuliaUnit.currentclass
    @test currentclass.name == "JuliaUnit.Internals"
end

# Case 3: Fail on purpose
@testcase "DummyFail" begin
    @test false
end

# Test counters
@testcase "NumberOfCases" begin
    @test JuliaUnit.currentclass.nbCases == 3
end

@testcase "NumberOfFailedCases" begin
    @test JuliaUnit.currentclass.nbFailures == 1
end

