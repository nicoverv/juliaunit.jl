using LightXML

@testclass "JuliaUnit.Internals"

@testcase "GenerateOutputFile" begin
    savesuite("tests.xml")
    try
        parse_file("tests.xml")
        @test true
    catch
        @test false
    end
end
