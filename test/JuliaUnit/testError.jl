@testcase "ThrowError" begin
    @testthrows sqrt(-1)
end
@testcase "ThrowSpecificError" begin
    @testthrowsspecific sqrt(-1) DomainError
end
@testcase "ThrowWrongError" begin
    @testthrowsspecific sqrt(-1) BoundsError
end
@testcase "ThrowNoError" begin
    @testthrows sqrt(1)
end
@testcase "ThrowNoErrorSpecific" begin
    @testthrowsspecific sqrt(1) BoundsError
end
    
