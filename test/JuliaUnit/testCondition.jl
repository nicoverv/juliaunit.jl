f = x -> x*x

@testcase "Test" begin
    @test f(2) > 3
end

@testcase "FailedTest" begin
    @test f(2) > 5
end
